package edu.cyut.javaclass;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner scanner=new Scanner(System.in);
        int num=scanner.nextInt();
        System.out.println("1+...+n+"+num+"的總和="+sum(num));
    }

    public static int sum(int n)
    {
        if(n==0)
        {
            return 0;
        }
        return n+sum(n-1);
    }
}